
package frc.robot;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;
import frc.robot.subsystems.ArcadeDrive;

import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Joystick
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import edu.wpi.first.wpilibj2.command.CommandScheduler;


public class Robot extends TimedRobot {
	CANSparkMax FrontRightMotor = new CANSparkMax(FrontRightPort, MotorType.kBrushless);
CANSparkMax FrontLeftMotor = new CANSparkMax(FrontLeftPort, MotorType.kBrushless);
CANSparkMax BackRightMotor = new CANSparkMax(BackRightPort, MotorType.kBrushless);
CANSparkMax BackLeftMotor = new CANSparkMax(BackLeftPort, MotorType.kBrushless);
Joystick driveStick = new Joystick();

 private final Timer timer = new Timer();











  public void robotInit() {

		FrontRightMotor.setInverted(true);
FrontLeftMotor.setInverted(false);
BackRightMotor.setInverted(true);
BackLeftMotor.setInverted(false);


MotorControllerGroup leftWheels = new MotorControllerGroup(FrontLeftMotor,BackLeftMotor);
MotorControllerGroup RightWheels = new MotorControllerGroup(FrontRightMotor, BackRightMotor);

DifferentialDrive differentialDrive = new DifferentialDrive(leftWheels, RightWheels);

        
}
  
    

   public void robotPeriodic() {
    CommandScheduler.getInstance().run();
  }

  public void disabledInit() {}

  public void disabledPeriodic() {}

  public void disabledExit() {}

  public void autonomousInit() {
    	timer.restart();


      }


  public void autonomousPeriodic() {}
	if(timer.get() < 10){
		mydrive.arcadeDrive(0.5, 0.0, false);

}
if(timer.get() > 10 && timer.get() < 11.5){
	// it takes about 1.5 seconds to move the robot 90 degrees right. This value is a guess
	mydrive.arcadeDrive(0.5, -0.5, false);

}


  
  public void autonomousExit() {
	}

    public void teleopInit() {
   
  }


  public void teleopPeriodic() {

	myDrive.arcadeDrive(-driveStick.getY(), -driveStick.getX());




}


  public void teleopExit() {
  
 	 }

  
  public void testInit() {
      }

  public void testPeriodic() {
}

  public void testExit() {

}
}



